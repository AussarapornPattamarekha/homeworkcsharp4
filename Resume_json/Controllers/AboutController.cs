﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Resume_json.Models;
using Resume_json.Services;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Resume_json.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AboutController : ControllerBase
    {
        public AboutController(JsonFileAboutService aboutService)
        {
            this.AboutService = aboutService;
        }
        public JsonFileAboutService AboutService { get; }

        [HttpGet]
        public IEnumerable<About> Get()
        {
            return AboutService.GetAbout();
        }
    }
}
