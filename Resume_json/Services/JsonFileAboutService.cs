﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Resume_json.Models;

namespace Resume_json.Services
{
    public class JsonFileAboutService
    {
        public JsonFileAboutService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        public IWebHostEnvironment WebHostEnvironment { get; }

        private string JsonFileName
        {
            get { return Path.Combine(WebHostEnvironment.WebRootPath, "data", "about.json"); }// C:\Users\iHAVECPU\source\repos\SampleWebsite\RazorAndData\wwwroot\data\products.json
        }

        public IEnumerable<About> GetAbout()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<About[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }

        public void EditAboutJS(string firstname, string lastname,string phone)
        {
            var abouts = GetAbout();

            var query = abouts.First();

            if (query.firstName != null)
            {
                query.firstName = firstname;
                query.lastName = lastname;
                query.phoneNumber = phone;
            }


            if (File.Exists(JsonFileName) == true)
            {
                File.Delete(JsonFileName);
            }

            using (var outputStream = File.Create(JsonFileName))
            {
                JsonSerializer.Serialize<IEnumerable<About>>(
                        new Utf8JsonWriter(outputStream, new JsonWriterOptions
                        {
                            SkipValidation = true,
                            Indented = true
                        }),
                        abouts
                    );
            }

        }
    }
}
