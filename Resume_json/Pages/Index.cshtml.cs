﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Resume_json.Models;
using Resume_json.Services;

namespace Resume_json.Pages
{
    public class IndexModel : PageModel
    {
        
        private readonly ILogger<IndexModel> _logger;
        public JsonFileAboutService AboutService;
        public IEnumerable<About> About
            ;

        public IndexModel(ILogger<IndexModel> logger , JsonFileAboutService _aboutService)
        {
            _logger = logger;
            AboutService = _aboutService;
        }

        public void OnGet()
        {
            About = AboutService.GetAbout();
        }
        
    }
}
