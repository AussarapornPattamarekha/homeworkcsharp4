﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Resume_json.Models
{
    public class About
    {
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String phoneNumber { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize<About>(this);
        }
    }
}
